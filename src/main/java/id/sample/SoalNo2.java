package id.sample;

import java.util.Arrays;
import java.util.HashMap;

public class SoalNo2 {
    public static void main(String[] args) {

        Point p0 = new Point(5,1);
        Point p1 = new Point(2,2);
        Point p2 = new Point(7,0);
        Point p3 = new Point(-1,-1);
        Point p4 = new Point(4,4);
        Point p5 = new Point(-8,-3);
        Point p6 = new Point(-4,-4);
        Point p7 = new Point(0,0);

        Point[] points = new Point[]{p0, p1, p2, p3, p4, p5, p6, p7};
        int radius = 4;

        Point resultPoint = findPointHavingMaximumCount(points, radius);
        System.out.println("Result: "+resultPoint.getX()+", "+ resultPoint.getY());
    }

    /*
           Soal 2:
           Write an algorithm that will print out the point that if become a center of a circle with a radius R,
           will have most of points within the circle.
    */
    public static Point findPointHavingMaximumCount(Point[] points, int radius){
        int maxValue = 0;
        Point pointHavingMaxValue = null;

        for(Point point : points){
            double[] xyPow2 = getPow2DistanceByCenterPoint(points, point);
            int result = countPointsWithinCircle(xyPow2, radius);
            if(result > maxValue) {
                maxValue = result;
                pointHavingMaxValue = point;
            }
        }

        return pointHavingMaxValue;
    }

    public static double[] getPow2DistanceByCenterPoint(Point points[], Point centerPoint){
        double xyPow2Result[] = new double[points.length];

        for (int i = 0; i < points.length; i++){
            double distanceX = Math.abs(centerPoint.getX() - points[i].getX());
            double distanceY = Math.abs(centerPoint.getY() - points[i].getY());

            xyPow2Result[i] = Math.pow(distanceX, 2) + Math.pow(distanceY, 2);
        }

        Arrays.sort(xyPow2Result);

        return xyPow2Result;
    }


    public static int countPointsWithinCircle(double xyPow2[], int radius) {
        int n = xyPow2.length;

        int leftIndex = 0, rightIndex = n - 1;

        while ((rightIndex - leftIndex) > 1) {
            int midIndex = (leftIndex + rightIndex) / 2;

            double currentDistance = Math.sqrt(xyPow2[midIndex]);

            if (currentDistance >= (radius * 1.0)) //outside or exactly on the circle line
                rightIndex = midIndex - 1;
            else
                leftIndex = midIndex;
        }

        double lastPositionDistance1 = Math.sqrt(xyPow2[leftIndex]);
        double lastPositionDistance2 = Math.sqrt(xyPow2[rightIndex]);

        if (lastPositionDistance1 >= (radius * 1.0))
            return 0;
        else if (lastPositionDistance2 < (radius * 1.0))
            return rightIndex + 1;
        else
            return leftIndex + 1;
    }

}