package id.sample;

import java.util.HashMap;

public class SoalNo1 {
    public static void main(String[] args) {
        //String[] stringArr = new String[]{"A", "C", "B", "B", "C", "C", "A", "B", "B", "C", "C", "A",};
        String[] stringArr = new String[]{"A", "C", "E", "B", "C", "C", "A", "B", "B", "C", "C", "A",};
        System.out.println(findFirstCharacter3Times(stringArr));
    }

    /*
       Soal 1:
       Given an array the size n, filled with a random character
       String[] stringArr = ["A", "C", "K", "B" .....];
       write an algorithm that will scan the array from the first element and print the first character that appear three times during scanning
   */
    static String findFirstCharacter3Times(String[] stringArr) {

        HashMap<String, Integer> data = new HashMap<>();
        for (int i = 0; i < stringArr.length; i++) {
            if (data.containsKey(stringArr[i])) {
                int val = data.get(stringArr[i]);
                val++;
                data.put(stringArr[i], val);

                if (data.get(stringArr[i]) >= 3)
                    return stringArr[i];
            } else
                data.put(stringArr[i], 1);
        }

        return "N/A";
    }
}
